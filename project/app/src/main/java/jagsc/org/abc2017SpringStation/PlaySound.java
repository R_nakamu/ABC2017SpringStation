package jagsc.org.abc2017SpringStation;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.media.SoundPool;

/**
 * Created by hiyuki on 2017/05/07.
 */

public class PlaySound {
	private MediaPlayer mediaPlayer;
	private AudioAttributes audioAttributes;
	private SoundPool soundPool;
	private int soundStart, soundFinish, soundCancel, soundCorrect, soundIncorrect, soundTap, soundIn_event;

	public void PlayMusic(Context context, int bgm){
		mediaPlayer = MediaPlayer.create(context, bgm);
		mediaPlayer.setLooping(true);
		mediaPlayer.start();
	}
	public void StopMusic(){
		if(mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
		}
		mediaPlayer.reset();
		mediaPlayer.release();
	}
	public void initSe(Context context) {
		audioAttributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_GAME).setContentType(AudioAttributes.CONTENT_TYPE_SPEECH).build();
		soundPool = new SoundPool.Builder().setAudioAttributes(audioAttributes).setMaxStreams(7).build();
		soundStart = soundPool.load(context, R.raw.start_button, 1);
		soundFinish = soundPool.load(context, R.raw.finish, 1);
		soundCancel = soundPool.load(context, R.raw.cancel, 1);
		soundCorrect = soundPool.load(context, R.raw.correct, 1);
		soundIncorrect = soundPool.load(context, R.raw.incorrect, 1);
		soundIn_event = soundPool.load(context, R.raw.in_event, 1);
		soundTap = soundPool.load(context, R.raw.tap, 1);
	}
	public void PlayStart(){
		soundPool.play(soundStart, 1.0f, 1.0f, 1, 0 ,1);
	}
	public void PlayFinish(){
		soundPool.play(soundFinish, 1.0f, 1.0f, 1, 0 ,1);
	}
	public void PlayCancel(){
		soundPool.play(soundCancel, 1.0f, 1.0f, 1, 0 ,1);
	}
	public void PlayCorrect(){
		soundPool.play(soundCorrect, 1.0f, 1.0f, 1, 0 ,1);
	}
	public void PlayIncorrect(){
		soundPool.play(soundIncorrect, 1.0f, 1.0f, 1, 0 ,1);
	}
	public void PlayIn_event(){
		soundPool.play(soundIn_event, 1.0f, 1.0f, 1, 0 ,1);
	}
	public void PlayTap(){
		soundPool.play(soundTap, 1.0f, 1.0f, 1, 0 ,1);
	}
	public void ReleaseSe(){
		soundPool.release();
	}
}
